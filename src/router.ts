import { createRouter, createWebHistory } from 'vue-router';
import HomePage from '@/views/HomePage.vue';
import WorksPage from '@/views/WorksPage.vue';
import AboutPage from '@/views/AboutPage.vue';
import ContactsPage from '@/views/ContactsPage.vue';

const routes = [
  { path: `${import.meta.env.BASE_URL}/`, component: HomePage },
  { path: `${import.meta.env.BASE_URL}/works`, component: WorksPage },
  { path: `${import.meta.env.BASE_URL}/about`, component: AboutPage },
  { path: `${import.meta.env.BASE_URL}/contacts`, component: ContactsPage },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
